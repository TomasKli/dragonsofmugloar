# README #
Application plays the http://www.dragonsofmugloar.com/api game.

To run the application, type in the command line** java -jar dragonsofmugloar-1.0-jar-with-dependencies.jar 5** (where 5 is the number of games to play)
The jar is added in the root project directory.

The application will log the results to console and also to a dragonsofmugloar.log file.