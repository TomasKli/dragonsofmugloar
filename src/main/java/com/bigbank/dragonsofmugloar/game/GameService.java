package com.bigbank.dragonsofmugloar.game;

import com.bigbank.dragonsofmugloar.game.entities.Dragon;
import com.bigbank.dragonsofmugloar.game.entities.Game;
import com.bigbank.dragonsofmugloar.game.entities.GameResult;

import java.util.Optional;

public interface GameService {

    Game startNewGame();

    GameResult solveGame(Long gameId, Optional<Dragon> dragon);
}
