package com.bigbank.dragonsofmugloar.game;

import com.bigbank.dragonsofmugloar.game.entities.Dragon;
import com.bigbank.dragonsofmugloar.game.entities.Game;
import com.bigbank.dragonsofmugloar.game.entities.GameResult;
import com.bigbank.dragonsofmugloar.utils.HttpService;
import com.bigbank.dragonsofmugloar.utils.PropertiesService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;

import java.io.IOException;
import java.util.Optional;

import static com.bigbank.dragonsofmugloar.utils.ApplicationPropertyKeys.GAME_SERVICE_NEW_GAME_URL;
import static com.bigbank.dragonsofmugloar.utils.ApplicationPropertyKeys.GAME_SERVICE_SOLUTION_URL;

public class WebGameService implements GameService {
    private static final int HTTP_OK_STATUS_CODE = 200;

    private HttpService httpService;

    public WebGameService(HttpService httpService) {
        this.httpService = httpService;
    }

    public Game startNewGame() {
        String URL = PropertiesService.getProperty(GAME_SERVICE_NEW_GAME_URL);
        HttpResponse response = httpService.httpGet(URL);
        verifyResponse(response);
        return mapResponse(response, Game.class);
    }

    public GameResult solveGame(Long gameId, Optional<Dragon> dragon) {
        String URL = PropertiesService.getProperty(GAME_SERVICE_SOLUTION_URL, gameId.toString());
        Header contentTypeHeader = new BasicHeader("Content-Type", "application/json");
        HttpResponse response = httpService.httpPut(URL, getEntity(dragon), contentTypeHeader);
        verifyResponse(response);
        return mapResponse(response, GameResult.class);
    }

    private StringEntity getEntity(Optional<Dragon> dragon) {
        try {
            if (!dragon.isPresent()) {
                return new StringEntity("{}");
            }
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, true);
            return new StringEntity(mapper.writeValueAsString(dragon.get()));
        } catch (IOException e) {
            throw new RuntimeException("Error communicating with the server");
        }
    }

    private <T> T mapResponse(HttpResponse response, Class<T> targetClass) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(response.getEntity().getContent(), targetClass);
        } catch (IOException e) {
            throw new RuntimeException("Error communicating with the server");
        }
    }

    private void verifyResponse(HttpResponse response) {
        if (response.getStatusLine().getStatusCode() != HTTP_OK_STATUS_CODE) {
            throw new RuntimeException("Error communicating with the server");
        }
    }

}