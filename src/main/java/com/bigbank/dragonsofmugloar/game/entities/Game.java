package com.bigbank.dragonsofmugloar.game.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Game {
    private Long gameId;
    private Knight knight;

    @Override
    public String toString() {
        return "Game{" +
                "gameId=" + gameId +
                ", knight=" + knight +
                '}';
    }
}
