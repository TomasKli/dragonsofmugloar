package com.bigbank.dragonsofmugloar.game.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Knight {

    private String name;
    private int attack;
    private int armor;
    private int agility;
    private int endurance;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Knight knight = (Knight) o;

        if (attack != knight.attack) return false;
        if (armor != knight.armor) return false;
        if (agility != knight.agility) return false;
        if (endurance != knight.endurance) return false;
        return name != null ? name.equals(knight.name) : knight.name == null;

    }

    @Override
    public String toString() {
        return "Knight{" +
                "name='" + name + '\'' +
                ", attack=" + attack +
                ", armor=" + armor +
                ", agility=" + agility +
                ", endurance=" + endurance +
                '}';
    }
}
