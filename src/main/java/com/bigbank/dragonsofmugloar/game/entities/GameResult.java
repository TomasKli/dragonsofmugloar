package com.bigbank.dragonsofmugloar.game.entities;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GameResult {

    private String status;
    private String message;

    @Override
    public String toString() {
        return "GameResult{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
