package com.bigbank.dragonsofmugloar.game.entities;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName("dragon")
public class Dragon {

    public static final Dragon ZEN_DRAGON = new Dragon(5, 5, 5, 5);
    public static final Dragon SHARP_CLAW_DRAGON = new Dragon(5, 10, 5, 0);

    private int scaleThickness;
    private int clawSharpness;
    private int wingStrength;
    private int fireBreath;

    @Override
    public String toString() {
        return "Dragon{" +
                "scaleThickness=" + scaleThickness +
                ", clawSharpness=" + clawSharpness +
                ", wingStrength=" + wingStrength +
                ", fireBreath=" + fireBreath +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dragon dragon = (Dragon) o;

        if (scaleThickness != dragon.scaleThickness) return false;
        if (clawSharpness != dragon.clawSharpness) return false;
        if (wingStrength != dragon.wingStrength) return false;
        return fireBreath == dragon.fireBreath;

    }
}
