package com.bigbank.dragonsofmugloar.weather;

import com.bigbank.dragonsofmugloar.utils.HttpService;
import com.bigbank.dragonsofmugloar.utils.PropertiesService;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import static com.bigbank.dragonsofmugloar.utils.ApplicationPropertyKeys.WEATHER_SERVICE_URL;

public class WebWeatherService implements WeatherService {
    Logger LOG = LoggerFactory.getLogger(WebWeatherService.class);

    private static final int HTTP_OK_STATUS_CODE = 200;
    private final static String WEATHER_CODE_ELEMENT_NAME = "code";

    private HttpService httpService;

    public WebWeatherService(HttpService httpService) {
        this.httpService = httpService;
    }

    public Weather getWeatherForGame(Long gameId) {
        String URL = PropertiesService.getProperty(WEATHER_SERVICE_URL, gameId.toString());
        HttpResponse response = httpService.httpGet(URL);
        String code = getWeatherCode(response);
        return code != null ? Weather.getWeather(code) : Weather.NORMAL;
    }

    private String getWeatherCode(HttpResponse response) {
        if (response.getStatusLine().getStatusCode() != HTTP_OK_STATUS_CODE) {
            return null;
        }
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(response.getEntity().getContent());
            NodeList nodeList = document.getElementsByTagName(WEATHER_CODE_ELEMENT_NAME);
            return nodeList.item(0).getTextContent();
        } catch (Exception e) {
            LOG.warn("Failed to communicate with weather service error: {}", e);
        }
        return null;
    }

}
