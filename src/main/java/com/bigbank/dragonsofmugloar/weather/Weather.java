package com.bigbank.dragonsofmugloar.weather;

import java.util.Arrays;

public enum Weather {
    STORM("SRO"),
    NORMAL("NMR"),
    HEAVY_RAIN("HVA"),
    LONG_DRY("T E"),
    FOG("FUNDEFINEDG");

    public String code;

    Weather(String code) {
        this.code = code;
    }

    public static Weather getWeather(String code) {
        return Arrays.stream(Weather.values())
                .filter(c -> c.code.equalsIgnoreCase(code))
                .findAny()
                .orElse(null);
    }
}
