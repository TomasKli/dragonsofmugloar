package com.bigbank.dragonsofmugloar.weather;

public interface WeatherService {

    Weather getWeatherForGame(Long gameId);
}
