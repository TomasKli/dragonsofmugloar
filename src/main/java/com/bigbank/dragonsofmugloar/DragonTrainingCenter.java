package com.bigbank.dragonsofmugloar;

import com.bigbank.dragonsofmugloar.game.entities.Dragon;
import com.bigbank.dragonsofmugloar.game.entities.Knight;
import com.bigbank.dragonsofmugloar.weather.Weather;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.bigbank.dragonsofmugloar.weather.Weather.FOG;
import static com.bigbank.dragonsofmugloar.weather.Weather.HEAVY_RAIN;
import static com.bigbank.dragonsofmugloar.weather.Weather.LONG_DRY;
import static com.bigbank.dragonsofmugloar.weather.Weather.STORM;

public class DragonTrainingCenter {
    private static final int SKILL_POINTS_PER_DRAGON = 20;
    private static final int FIRST_SKILL_OFFSET = 2;
    private static final int SECOND_SKILL_OFFSET = -1;
    private static final int THIRD_SKILL_OFFSET = -1;

    private static final String KNIGHTS_ARMOR = "ARMOR";
    private static final String KNIGHTS_ATTACK = "ATTACK";
    private static final String KNIGHTS_AGILITY = "AGILITY";
    private static final String KNIGHTS_ENDURANCE = "ENDURANCE";

    public DragonTrainingCenter() {
    }

    public Optional<Dragon> prepareDragon(Knight knight, Weather weather) {
        if (STORM.equals(weather)) {
            return Optional.empty();
        } else if (HEAVY_RAIN.equals(weather)) {
            return Optional.of(Dragon.SHARP_CLAW_DRAGON);
        } else if (LONG_DRY.equals(weather) || FOG.equals(weather)) {
            return Optional.of(Dragon.ZEN_DRAGON);
        }
        return Optional.of(prepareDragonAgainst(knight));
    }

    private Dragon prepareDragonAgainst(Knight knight) {
        Map<String, Integer> knightsStats = getKnightsStats(knight);
        Dragon dragon = new Dragon();
        int pointsLeft = SKILL_POINTS_PER_DRAGON;

        pointsLeft -= matchStrongestSkillBy(FIRST_SKILL_OFFSET, knightsStats, dragon);
        pointsLeft -= matchStrongestSkillBy(SECOND_SKILL_OFFSET, knightsStats, dragon);
        pointsLeft -= matchStrongestSkillBy(THIRD_SKILL_OFFSET, knightsStats, dragon);
        setDragonProperty(dragon, getStrongestSkillFrom(knightsStats), pointsLeft);
        return dragon;
    }

    private int matchStrongestSkillBy(int matchBy, Map<String, Integer> knightsStats, Dragon dragon) {
        String knightsSkill = getStrongestSkillFrom(knightsStats);
        int skillPointsNeeded = knightsStats.get(knightsSkill) + matchBy;
        setDragonProperty(dragon, knightsSkill, skillPointsNeeded);
        knightsStats.remove(knightsSkill);
        return skillPointsNeeded;
    }

    private String getStrongestSkillFrom(Map<String, Integer> knightsStats) {
        return Collections.max(knightsStats.entrySet(), (e1, e2) -> e1.getValue() - e2.getValue()).getKey();
    }

    private void setDragonProperty(Dragon dragon, String knightsProperty, int skillPoints) {
        if (KNIGHTS_ARMOR.equals(knightsProperty)) {
            dragon.setClawSharpness(skillPoints);
        } else if (KNIGHTS_ATTACK.equals(knightsProperty)) {
            dragon.setScaleThickness(skillPoints);
        } else if (KNIGHTS_AGILITY.equals(knightsProperty)) {
            dragon.setWingStrength(skillPoints);
        } else if (KNIGHTS_ENDURANCE.equals(knightsProperty)) {
            dragon.setFireBreath(skillPoints);
        }
    }

    private Map<String, Integer> getKnightsStats(Knight knight) {
        Map<String, Integer> stats = new HashMap<>();
        stats.put(KNIGHTS_ARMOR, knight.getArmor());
        stats.put(KNIGHTS_ATTACK, knight.getAttack());
        stats.put(KNIGHTS_AGILITY, knight.getAgility());
        stats.put(KNIGHTS_ENDURANCE, knight.getEndurance());
        return stats;
    }
}
