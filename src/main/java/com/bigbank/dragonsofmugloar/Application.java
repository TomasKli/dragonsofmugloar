package com.bigbank.dragonsofmugloar;

import com.bigbank.dragonsofmugloar.game.GameService;
import com.bigbank.dragonsofmugloar.game.WebGameService;
import com.bigbank.dragonsofmugloar.utils.HttpService;
import com.bigbank.dragonsofmugloar.utils.HttpServiceImpl;
import com.bigbank.dragonsofmugloar.weather.WeatherService;
import com.bigbank.dragonsofmugloar.weather.WebWeatherService;

public class Application {

    public static void main(String[] args) {
        DragonsOfMugloarGame application = setUpApplication();
        application.play(getGamesCount(args));
    }

    private static DragonsOfMugloarGame setUpApplication() {
        HttpService httpService = new HttpServiceImpl();
        GameService gameService = new WebGameService(httpService);
        WeatherService weatherService = new WebWeatherService(httpService);
        return new DragonsOfMugloarGame(gameService, weatherService, new DragonTrainingCenter());
    }

    private static int getGamesCount(String[] args) {
        int gameCount = 50;
        if (args.length > 0) {
            try {
                gameCount = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                throw new RuntimeException("Invalid parameter: " + args[0]);
            }
        }
        return gameCount;
    }

}
