package com.bigbank.dragonsofmugloar.utils;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;


public interface HttpService {

    HttpResponse httpGet(String url);

    HttpResponse httpPut(String url, StringEntity entity, Header... headers);
}
