package com.bigbank.dragonsofmugloar.utils;

public class ApplicationPropertyKeys {

    public static final String GAME_SERVICE_NEW_GAME_URL = "game.service.new.game.url";
    public static final String GAME_SERVICE_SOLUTION_URL = "game.service.solution.url";
    public static final String WEATHER_SERVICE_URL = "weather.service.url";
}
