package com.bigbank.dragonsofmugloar.utils;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class HttpServiceImpl implements HttpService {

    public HttpResponse httpGet(String url)  {
        return executeRequest(new HttpGet(url));
    }

    public HttpResponse httpPut(String url, StringEntity entity, Header... headers)  {
        HttpPut request = new HttpPut(url);
        request.setHeaders(headers);
        request.setEntity(entity);
        return executeRequest(request);
    }

    private HttpResponse executeRequest(HttpRequestBase request)  {
        try {
            HttpClient client = HttpClientBuilder.create().build();
            return client.execute(request);
        } catch (IOException e) {
            throw new RuntimeException("Error communicating with the server");
        }
    }
}
