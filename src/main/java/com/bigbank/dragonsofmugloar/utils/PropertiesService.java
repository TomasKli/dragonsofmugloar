package com.bigbank.dragonsofmugloar.utils;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

public class PropertiesService {

    private final static String PROPERTIES_FILE_NAME = "application.properties";
    private static Properties properties = null;

    private PropertiesService() {
    }

    public static String getProperty(String key) {
        if (properties == null) {
            loadProperties();
        }
        return properties.getProperty(key);
    }

    public static String getProperty(String key, String param) {
        String value = getProperty(key);
        return MessageFormat.format(value, param);
    }

    private static void loadProperties() {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try (InputStream stream = loader.getResourceAsStream(PROPERTIES_FILE_NAME)) {
            properties = new Properties();
            properties.load(stream);
        } catch (IOException ex) {
            throw new RuntimeException("Failed to load application properties file");
        }
    }
}
