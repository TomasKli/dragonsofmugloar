package com.bigbank.dragonsofmugloar;

import com.bigbank.dragonsofmugloar.game.GameService;
import com.bigbank.dragonsofmugloar.game.entities.Dragon;
import com.bigbank.dragonsofmugloar.game.entities.Game;
import com.bigbank.dragonsofmugloar.game.entities.GameResult;
import com.bigbank.dragonsofmugloar.weather.Weather;
import com.bigbank.dragonsofmugloar.weather.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class DragonsOfMugloarGame {
    Logger LOG = LoggerFactory.getLogger(DragonsOfMugloarGame.class);
    public static final String VICTORY_STATUS = "Victory";

    private GameService gameService;
    private WeatherService weatherService;
    private DragonTrainingCenter trainingCenter;

    public DragonsOfMugloarGame(GameService gameService, WeatherService weatherService, DragonTrainingCenter trainingCenter) {
        this.gameService = gameService;
        this.weatherService = weatherService;
        this.trainingCenter = trainingCenter;
    }

    public void play(int numOfGames) {
        int victories = 0;
        for (int i = 1; i <= numOfGames; i++) {
            GameResult result = playGame();
            if (VICTORY_STATUS.equalsIgnoreCase(result.getStatus())) {
                victories++;
            }
        }
        double winPercentage = (victories / numOfGames) * 100;
        logResult(numOfGames, victories, winPercentage);
    }

    private GameResult playGame() {
        Game game = gameService.startNewGame();
        Weather weather = weatherService.getWeatherForGame(game.getGameId());
        Optional<Dragon> dragon = trainingCenter.prepareDragon(game.getKnight(), weather);
        GameResult result = gameService.solveGame(game.getGameId(), dragon);
        logGameResult(game, dragon, result);
        return result;
    }

    private void logGameResult(Game game, Optional<Dragon> dragon, GameResult result) {
        String dragonString = dragon.isPresent() ? dragon.get().toString() : "No dragon sent";
        LOG.info("\n{}\n{}\n{}\n", game, dragonString, result);
    }

    private void logResult(int numOfGames, int victories, double winPercentage) {
        LOG.info("**************************************************");
        LOG.info("Games played {}, games won {}. Winning rate: ({}%)", numOfGames, victories, winPercentage);
        LOG.info("**************************************************");
    }
}
