package com.bigbank.dragonsofmugloar;

import com.bigbank.dragonsofmugloar.game.entities.Game;
import com.bigbank.dragonsofmugloar.game.entities.Knight;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;

public class BaseTest {

    private static final String KNIGTS_NAME = "Sir Knight of Testhood";

    protected Game getGame(Long gameId) {
        Game game = new Game();
        game.setGameId(gameId);
        return game;
    }

    protected Knight getKnight() {
        return new Knight(KNIGTS_NAME, 5, 5, 5, 5);
    }

    protected Knight getKnight(int armor, int attack, int agility, int endurance) {
        return new Knight(KNIGTS_NAME, armor, attack, agility, endurance);
    }

    protected HttpResponse constructResponse(int statisCode, String body) {
        StatusLine statusLine = new BasicStatusLine(new ProtocolVersion("", 0, 1), statisCode, "test reason");
        HttpEntity entity = new StringEntity(body, "UTF-8");
        HttpResponse response = new BasicHttpResponse(statusLine);
        response.setEntity(entity);
        return response;
    }

}
