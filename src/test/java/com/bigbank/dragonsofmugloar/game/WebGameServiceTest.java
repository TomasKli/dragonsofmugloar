package com.bigbank.dragonsofmugloar.game;

import com.bigbank.dragonsofmugloar.BaseTest;
import com.bigbank.dragonsofmugloar.game.entities.Dragon;
import com.bigbank.dragonsofmugloar.game.entities.Game;
import com.bigbank.dragonsofmugloar.game.entities.GameResult;
import com.bigbank.dragonsofmugloar.utils.HttpService;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class WebGameServiceTest extends BaseTest {

    private static final String JSON_NEW_GAME_RESPONSE = "{ \"gameId\": 6488520,\"knight\": " +
            "{\"name\": \"Sir Knight of Testhood\",\"attack\": 2,\"armor\": 8,\"agility\": 8,\"endurance\": 2}}";

    private static final String JSON_SOLVE_GAME_RESPONSE = "{\"status\": \"Victory\",\"message\": \"Dragon was successful in a glorious battle\"}";

    private HttpService httpService = Mockito.mock(HttpService.class);
    private WebGameService webGameService;

    @Before
    public void setup() {
        webGameService = new WebGameService(httpService);
    }

    @Test
    public void start_new_game_constructs_game_object_from_json_string() {
        HttpResponse response = constructResponse(200, JSON_NEW_GAME_RESPONSE);
        when(httpService.httpGet(any(String.class))).thenReturn(response);

        Game game = webGameService.startNewGame();

        assertEquals(Long.valueOf(6488520), game.getGameId());
        assertEquals(getKnight(2, 8, 8, 2), game.getKnight());
    }

    @Test
    public void start_new_game_throws_exception_if_invalid_response_code_received() {
        HttpResponse response = constructResponse(404, "");
        when(httpService.httpGet(any(String.class))).thenReturn(response);
        try {
            webGameService.startNewGame();
        } catch (RuntimeException e) {
            assertEquals("Error communicating with the server", e.getMessage());
            return;
        }
        fail();
    }

    @Test
    public void solve_game_returns_game_result_from_json_response() {
        HttpResponse response = constructResponse(200, JSON_SOLVE_GAME_RESPONSE);
        when(httpService.httpPut(any(String.class), any(StringEntity.class), any(Header[].class))).thenReturn(response);

        GameResult result = webGameService.solveGame(1l, Optional.of(Dragon.ZEN_DRAGON));

        assertEquals("Victory", result.getStatus());
        assertEquals("Dragon was successful in a glorious battle", result.getMessage());
    }

    @Test
    public void solve_game_throws_exception_if_invalid_response_code_received() {
        HttpResponse response = constructResponse(404, "");
        when(httpService.httpPut(any(String.class), any(StringEntity.class), any(Header[].class))).thenReturn(response);
        try {
            webGameService.solveGame(1l, Optional.of(Dragon.ZEN_DRAGON));
        } catch (RuntimeException e) {
            assertEquals("Error communicating with the server", e.getMessage());
            return;
        }
        fail();
    }
}