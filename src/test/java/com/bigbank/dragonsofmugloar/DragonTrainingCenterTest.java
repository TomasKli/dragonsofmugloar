package com.bigbank.dragonsofmugloar;

import com.bigbank.dragonsofmugloar.game.entities.Dragon;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static com.bigbank.dragonsofmugloar.game.entities.Dragon.SHARP_CLAW_DRAGON;
import static com.bigbank.dragonsofmugloar.game.entities.Dragon.ZEN_DRAGON;
import static com.bigbank.dragonsofmugloar.weather.Weather.FOG;
import static com.bigbank.dragonsofmugloar.weather.Weather.HEAVY_RAIN;
import static com.bigbank.dragonsofmugloar.weather.Weather.LONG_DRY;
import static com.bigbank.dragonsofmugloar.weather.Weather.NORMAL;
import static com.bigbank.dragonsofmugloar.weather.Weather.STORM;
import static org.junit.Assert.assertEquals;

public class DragonTrainingCenterTest extends BaseTest {

    DragonTrainingCenter trainingCenter;

    @Before
    public void setup() {
        trainingCenter = new DragonTrainingCenter();
    }

    @Test
    public void zen_dragons_are_picked_for_dry_weather() {
        Optional<Dragon> dragon = trainingCenter.prepareDragon(getKnight(), LONG_DRY);
        assertEquals(ZEN_DRAGON, dragon.get());
    }

    @Test
    public void zen_dragons_are_picked_when_weather_is_foggy() {
        Optional<Dragon> dragon = trainingCenter.prepareDragon(getKnight(), FOG);
        assertEquals(ZEN_DRAGON, dragon.get());
    }

    @Test
    public void sharp_claw_dragons_are_picked_for_rainy_weather() {
        Optional<Dragon> dragon = trainingCenter.prepareDragon(getKnight(), HEAVY_RAIN);
        assertEquals(SHARP_CLAW_DRAGON, dragon.get());
    }

    @Test
    public void no_dragons_are_picked_when_there_is_a_storm() {
        Optional<Dragon> dragon = trainingCenter.prepareDragon(getKnight(), STORM);
        assertEquals(Optional.empty(), dragon);
    }

    @Test
    public void dragon_matches_strongest_knights_skill_by_2_points() {
        Optional<Dragon> dragon;
        dragon = trainingCenter.prepareDragon(getKnight(7, 6, 5, 2), NORMAL);
        assertEquals(9, dragon.get().getScaleThickness());

        dragon = trainingCenter.prepareDragon(getKnight(5, 8, 5, 2), NORMAL);
        assertEquals(10, dragon.get().getClawSharpness());

        dragon = trainingCenter.prepareDragon(getKnight(4, 5, 6, 5), NORMAL);
        assertEquals(8, dragon.get().getWingStrength());

        dragon = trainingCenter.prepareDragon(getKnight(7, 3, 2, 8), NORMAL);
        assertEquals(10, dragon.get().getFireBreath());
    }

    @Test
    public void dragon_matches_third_strongest_knight_skill_with_one_point_less() {
        Optional<Dragon> dragon;
        dragon = trainingCenter.prepareDragon(getKnight(5, 7, 6, 2), NORMAL);
        assertEquals(4, dragon.get().getScaleThickness());

        dragon = trainingCenter.prepareDragon(getKnight(7, 5, 6, 2), NORMAL);
        assertEquals(4, dragon.get().getClawSharpness());

        dragon = trainingCenter.prepareDragon(getKnight(6, 3, 4, 7), NORMAL);
        assertEquals(3, dragon.get().getWingStrength());

        dragon = trainingCenter.prepareDragon(getKnight(6, 3, 7, 4), NORMAL);
        assertEquals(3, dragon.get().getFireBreath());
    }

    @Test
    public void dragon_matches_last_knight_skill_by_knights_skill_sum_subtracted_from_20() {
        Optional<Dragon> dragon;
        dragon = trainingCenter.prepareDragon(getKnight(2, 6, 5, 7), NORMAL);
        assertEquals(2, dragon.get().getScaleThickness());

        dragon = trainingCenter.prepareDragon(getKnight(5, 2, 5, 8), NORMAL);
        assertEquals(2, dragon.get().getClawSharpness());

        dragon = trainingCenter.prepareDragon(getKnight(6, 5, 4, 5), NORMAL);
        assertEquals(4, dragon.get().getWingStrength());

        dragon = trainingCenter.prepareDragon(getKnight(8, 8, 2, 0), NORMAL);
        assertEquals(2, dragon.get().getFireBreath());
    }
}