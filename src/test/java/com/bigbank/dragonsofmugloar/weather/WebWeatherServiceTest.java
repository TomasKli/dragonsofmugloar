package com.bigbank.dragonsofmugloar.weather;

import com.bigbank.dragonsofmugloar.BaseTest;
import com.bigbank.dragonsofmugloar.utils.HttpService;
import org.apache.http.HttpResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static com.bigbank.dragonsofmugloar.weather.Weather.FOG;
import static com.bigbank.dragonsofmugloar.weather.Weather.HEAVY_RAIN;
import static com.bigbank.dragonsofmugloar.weather.Weather.LONG_DRY;
import static com.bigbank.dragonsofmugloar.weather.Weather.NORMAL;
import static com.bigbank.dragonsofmugloar.weather.Weather.STORM;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class WebWeatherServiceTest extends BaseTest {

    private static final String XML_NORMAL_WEATHER_RESPONSE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><code>" + NORMAL.code + "</code>";
    private static final String XML_HEAVY_RAIN_RESPONSE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><code>" + HEAVY_RAIN.code + "</code>";
    private static final String XML_STORM_RESPONSE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><code>" + STORM.code + "</code>";
    private static final String XML_FOG_RESPONSE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><code>" + FOG.code + "</code>";
    private static final String XML_LONG_DRY_RESPONSE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><code>" + LONG_DRY.code + "</code>";

    private HttpService httpService = Mockito.mock(HttpService.class);
    private WebWeatherService webWeatherService;

    @Before
    public void setup() {
        webWeatherService = new WebWeatherService(httpService);
    }

    @Test
    public void storm_weather_is_parsed_from_xml_response() {
        weather_is_parsed_from_received_xml(XML_STORM_RESPONSE, STORM);
    }

    @Test
    public void dry_weather_is_parsed_from_xml_response() {
        weather_is_parsed_from_received_xml(XML_LONG_DRY_RESPONSE, LONG_DRY);
    }

    @Test
    public void rainy_weather_is_parsed_from_xml_response() {
        weather_is_parsed_from_received_xml(XML_HEAVY_RAIN_RESPONSE, HEAVY_RAIN);
    }

    @Test
    public void foggy_weather_is_parsed_from_xml_response() {
        weather_is_parsed_from_received_xml(XML_FOG_RESPONSE, FOG);
    }

    @Test
    public void normal_weather_selected_if_received_invalid_response() {
        HttpResponse response = constructResponse(404, "");
        when(httpService.httpGet(any(String.class))).thenReturn(response);
        Weather weather = webWeatherService.getWeatherForGame(1l);

        assertEquals(NORMAL, weather);
    }


    private void weather_is_parsed_from_received_xml(String xmlResponse, Weather expectedWeather) {
        HttpResponse response = constructResponse(200, xmlResponse);
        when(httpService.httpGet(any(String.class))).thenReturn(response);
        Weather weather = webWeatherService.getWeatherForGame(1l);
        assertEquals(expectedWeather, weather);
    }
}