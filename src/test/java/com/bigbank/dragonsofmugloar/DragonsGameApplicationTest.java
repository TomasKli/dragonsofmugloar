package com.bigbank.dragonsofmugloar;

import com.bigbank.dragonsofmugloar.game.GameService;
import com.bigbank.dragonsofmugloar.game.entities.Game;
import com.bigbank.dragonsofmugloar.game.entities.GameResult;
import com.bigbank.dragonsofmugloar.game.entities.Knight;
import com.bigbank.dragonsofmugloar.weather.Weather;
import com.bigbank.dragonsofmugloar.weather.WeatherService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DragonsGameApplicationTest extends BaseTest {

    private DragonsOfMugloarGame gameApplication;

    private GameService gameService = Mockito.mock(GameService.class);
    private WeatherService weatherService = Mockito.mock(WeatherService.class);
    private DragonTrainingCenter trainingCenter = Mockito.mock(DragonTrainingCenter.class);

    @Before
    public void setup() {
        gameApplication = new DragonsOfMugloarGame(gameService, weatherService, trainingCenter);
    }

    @Test
    public void game_is_played_a_specified_amount_of_time(){
        int gameCount = 10;
        Game game = getGame(1l);
        when(gameService.startNewGame()).thenReturn(game);
        when(trainingCenter.prepareDragon(any(Knight.class), any(Weather.class))).thenReturn(Optional.empty());
        when(gameService.solveGame(any(Long.class), any(Optional.class))).thenReturn(new GameResult());

        gameApplication.play(gameCount);

        verify(gameService, times(gameCount)).startNewGame();
        verify(weatherService, times(gameCount)).getWeatherForGame(1l);
        verify(gameService, times(gameCount)).solveGame(any(Long.class), any(Optional.class));
        verify(trainingCenter, times(gameCount)).prepareDragon(any(Knight.class), any(Weather.class));
    }
}